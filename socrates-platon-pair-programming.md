# Socrates et Platon font du pair programming

Petite approche philosophique d'un point de vue personnel sur le pair programming.

![Voici la tronche de Platon](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Plato_Silanion_Musei_Capitolini_MC1377.jpg/220px-Plato_Silanion_Musei_Capitolini_MC1377.jpg)

La première fois que j'ai entendu parler de [pair programming](http://www.extremeprogramming.org/rules/pair.html) était pendant mes études, dans un cours de développement de projets, pour lequel l'objectif était de construire un projet en équipe en utilisant le [Extreme Programming](http://www.extremeprogramming.org/) (XP).


**Mais qu'est que c'est ?**

Le pair programming consiste à travailler sur un problème à 2 en utilisant 1 seul écran (ou poste).

On peut également brancher plusieurs écrans, claviers et souris si besoin (merci USB!).

C'est également une best practice dans les entreprises agiles d'haute performance dans le monde du DevOps:

pour avoir un feedback plus rapidement (The Second Way)
pour l'amélioration et l'apprentissage continus (The Third Way)


**La paradoxe**

Cette équipe va produire un code source avec beaucoup plus de qualité que en travaillant séparément, en respectant le délais de livraison.

Bon, ceci est vrai seulement si l'on n'utilise pas le pair programming pour faire du mentoring (relation professeur-élève), chose qui est possible et marche très bien pour une montée en compétences rapide.

Le pair programming nous apporte donc plusieurs avantages, je vais en énumérer quelques uns:

1. la qualité du résultat s'incrémente
2. il y a une radiation et partage de connaissances...
3. ... qui a par conséquence l'apprentissage, par
3.1. des nouveautés
3.2. des points de vue différents
3.3. des échecs
4. l'amélioration de l'esprit d'équipe et ses liens
5. bien-être au travail

Cela peut prendre un peu de temps à s'y habituer, mais le pair programming s'apprend, ce n'est pas inné!


**Et on arrive aux philosophes...**

Si on prends un peu de recul, on voit que cette liste nous parle principalement d'échanger avec d'autres personnes, de confronter des points de vue, de discuter, de **dialoguer** pour trouver la meilleure solution ou la **vrai** solution à un problème.

Il y a presque 2500 ans, [Socrates](https://en.wikipedia.org/wiki/Socrates) (469-339 A.C.) s'est déjà rendu compte de cela et a inventé le dialogue comme moyen pour trouver la vérité : il croyait que pour trouver la vérité il fallait poser des questions. Personnellement, je suis preneur.

Ce n'est pas pour rien que son élève par excellence, [Platon](https://en.wikipedia.org/wiki/Plato) (427-347 A.C.), a écrit ses connus Dialogues, avec Socrates comme personnage principal.

Platon a écrit beaucoup d'œuvres et également le [Mythe de la Caverne](https://fr.wikipedia.org/wiki/All%C3%A9gorie_de_la_caverne).

Grosso merdo, imaginez des esclaves dans une grotte, enchainés dos vers la sortie de la grotte.

Tout ce qu'ils connaissent c'est l'intérieur de la grotte et les ombres projetées sur les murs: ils prennent pour vérité l'environnement qu'ils connaissent.

Un jour, un des esclaves est libéré et obligé de sortir de la grotte, et il se rends compte qu'il existe une autre réalité.

C'est grâce à ce changement obligé qu'il a trouvé une autre vérité.


**Pour en finir**

Le pair programming est considéré une best practice dans les entreprises agiles d'haute performance dans le monde du DevOps:

* pour avoir un feedback plus rapidement (The Second Way)
* pour l'amélioration et l'apprentissage continus (The Third Way)

C'est quelqu'un qui nous secoue, qui nous challenge, qui dialogue avec nous, qui va faire qu'on sorte de la grotte et qu'on trouve la meilleure solution à un problème donné.

Comme toujours, du travail en équipe. Merci pair programming, Socrates et Platon !

